
import os
import pickle

from tensorflow import keras
import pandas as pd
import numpy as np
import tensorflow as tf
from keras.utils import to_categorical
from sklearn import preprocessing
from sklearn.metrics import classification_report
from sklearn.preprocessing import StandardScaler
from tensorflow.python.keras import Sequential, Input
from tensorflow.python.keras.layers import Flatten, Dense, Dropout, Conv1D, LSTM, Bidirectional, Concatenate, \
    MaxPooling1D, AveragePooling1D, TimeDistributed, GlobalAvgPool1D, GlobalMaxPool1D, multiply, average, AvgPool1D, \
    concatenate
from tensorflow.python.keras.callbacks import EarlyStopping, LearningRateScheduler, ModelCheckpoint
from tensorflow.python.keras.models import load_model, Model
from tensorflow.python.keras.layers import Lambda
from tensorflow.python.keras.backend import max




def read_pickle_data(path=None):
    if path is None or not os.path.isfile(path):
        return None
    with open(path, 'rb') as fid:
        data = pickle.load(fid)
        fid.close()
        return data


def mkdir(path):
    if not os.path.exists(path):
        try:
            os.makedirs(path)
        except NotADirectoryError:
            pass


def multichannel_network():
    input_layer = Input((403, 1), name="Input3d_Conv")

    # block 1
    y = Conv1D(filters=32, kernel_size=1, activation='relu')(input_layer)
    y = Conv1D(filters=16, kernel_size=1, activation='relu', padding='same')(y)
    y_ = Dropout(0.4)(y)
    y = MaxPooling1D(pool_size=1)(y_)
    # y_ = AveragePooling1D(pool_size=1)(y_)
    # y = Concatenate()([y, y_])
    y = Flatten()(y)

    # block 2
    z = Conv1D(filters=32, kernel_size=2, activation='relu')(input_layer)
    z = Conv1D(filters=16, kernel_size=2, activation='relu', padding='same')(z)
    z_ = Dropout(0.4)(z)
    z = MaxPooling1D(pool_size=1)(z_)
    # z_ = AveragePooling1D(pool_size=1)(z_)
    # z = Concatenate()([z, z_])
    z = Flatten()(z)

    # block 3
    # # y=Dropout(0.1)(y)
    x = Conv1D(filters=32, kernel_size=3, activation='relu')(input_layer)
    x = Conv1D(filters=16, kernel_size=3, activation='relu', padding='same')(x)
    x_ = Dropout(0.4)(x)
    x = MaxPooling1D(pool_size=1)(x_)
    # x_ = AveragePooling1D(pool_size=1)(x_)
    # x = Concatenate()([x, x_])
    x = Flatten()(x)

    # block 4
    m = Conv1D(filters=32, kernel_size=4, activation='relu')(input_layer)
    m = Conv1D(filters=16, kernel_size=4, activation='relu', padding='same')(m)
    m_ = Dropout(0.4)(m)
    m = MaxPooling1D(pool_size=1)(m_)
    # m_ = AveragePooling1D(pool_size=1)(m_)
    # m = Concatenate()([m, m_])
    m = Flatten()(m)

    k = Concatenate()([y, z, x, m])

    y = Dropout(0.5)(k)

    x = Dense(128, activation='relu')(y)
    y = Dropout(0.5)(x)

    x = Dense(64, activation='relu')(y)
    x = Dropout(0.5)(x)

    x = Dense(3, activation='softmax')(x)
    model = keras.models.Model(inputs=input_layer, outputs=x)
    opt = keras.optimizers.Adam(lr=0.0001)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['acc'])
    return model


def model1():
    input_layer = Input((403, 1), name="input_c1")

    # block 1
    y = Conv1D(filters=32, kernel_size=1, activation='relu', name='c1_conv1')(input_layer)
    y = Conv1D(filters=16, kernel_size=1, activation='relu', padding='same', name='c1_conv2')(y)
    y_ = Dropout(0.4, name='c1_dropout1')(y)
    y = MaxPooling1D(pool_size=1, name='c1_mpool1')(y_)
    y = Flatten(name='c1_flatten1')(y)
    y = Dropout(0.5, name='c1_dropout2')(y)
    x = Dense(128, activation='relu', name='c1_dense1')(y)
    y = Dropout(0.5, name='c1_dropout3')(x)

    x = Dense(64, activation='relu', name='c1_dense2')(y)
    x = Dropout(0.5, name='c1_dropout4')(x)

    x = Dense(3, activation='softmax', name='c1_softmax')(x)
    model = keras.models.Model(inputs=input_layer, outputs=x)
    opt = keras.optimizers.Adam(lr=0.0001)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['acc'])
    return model


def model2():
    input_layer = Input((403, 1), name="input_c2")
    # block 2
    z = Conv1D(filters=32, kernel_size=2, activation='relu', name='c2_conv1')(input_layer)
    z = Conv1D(filters=16, kernel_size=2, activation='relu', padding='same', name='c2_conv2')(z)
    z_ = Dropout(0.4, name='c2_dropout1')(z)
    z = MaxPooling1D(pool_size=1, name='c2_mpool1')(z_)
    # z_ = AveragePooling1D(pool_size=1)(z_)
    # z = Concatenate()([z, z_])
    z = Flatten(name='c2_flatten1')(z)

    y = Dropout(0.5, name='c2_dropout2')(z)

    x = Dense(128, activation='relu', name='c2_dense1')(y)
    y = Dropout(0.5, name='c2_dropout3')(x)

    x = Dense(64, activation='relu', name='c2_dense2')(y)
    x = Dropout(0.5, name='c2_dropout4')(x)

    x = Dense(3, activation='softmax', name='c2_softmax')(x)
    model = keras.models.Model(inputs=input_layer, outputs=x)
    opt = keras.optimizers.Adam(lr=0.0001)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['acc'])
    return model


def model3():
    input_layer = Input((403, 1), name="input_c3")

    # block 3
    # # y=Dropout(0.1)(y)
    x = Conv1D(filters=32, kernel_size=3, activation='relu', name='c3_conv1')(input_layer)
    x = Conv1D(filters=16, kernel_size=3, activation='relu', padding='same', name='c3_conv2')(x)
    x_ = Dropout(0.4, name='c3_dropout1')(x)
    x = MaxPooling1D(pool_size=1, name='c3_mpool1')(x_)
    # x_ = AveragePooling1D(pool_size=1)(x_)
    # x = Concatenate()([x, x_])
    x = Flatten(name='c3_flatten1')(x)
    x = Dropout(0.5, name='c3_dropout2')(x)
    x = Dense(128, activation='relu', name='c3_dense1')(x)
    y = Dropout(0.5, name='c3_dropout3')(x)

    x = Dense(64, activation='relu', name='c3_dense2')(y)
    x = Dropout(0.5, name='c3_dropout4')(x)

    x = Dense(3, activation='softmax', name='c3_softmax')(x)
    model = keras.models.Model(inputs=input_layer, outputs=x)
    opt = keras.optimizers.Adam(lr=0.0001)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['acc'])
    return model


def model4():
    input_layer = Input((403, 1), name="input_c4")

    # block 4
    m = Conv1D(filters=32, kernel_size=4, activation='relu', name='c4_conv1')(input_layer)
    m = Conv1D(filters=16, kernel_size=4, activation='relu', padding='same', name='c4_conv2')(m)
    m_ = Dropout(0.4, name='c4_dropout1')(m)
    m = MaxPooling1D(pool_size=1, name='c4_mpool')(m_)
    # m_ = AveragePooling1D(pool_size=1)(m_)
    # m = Concatenate()([m, m_])
    m = Flatten(name='c4_flatten1')(m)

    y = Dropout(0.5, name='c4_dropout2')(m)

    x = Dense(128, activation='relu', name='c4_dense1')(y)
    y = Dropout(0.5, name='c4_dropout3')(x)

    x = Dense(64, activation='relu', name='c4_dense2')(y)
    x = Dropout(0.5, name='c4_dropout4')(x)

    x = Dense(3, activation='softmax', name='c4_softmax')(x)
    model = keras.models.Model(inputs=input_layer, outputs=x)
    opt = keras.optimizers.Adam(lr=0.0001)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['acc'])
    return model


def combined_model(set):
    # design four models
    input_layer = Input((403, 1))

    # load the models
    c1 = load_model(set + '/c1.h5')
    c2 = load_model(set + '/c2.h5')
    c3 = load_model(set + '/c3.h5')
    c4 = load_model(set + '/c4.h5')
    # print(c1.input)

    # Load channel summary
    print(c1.summary())
    print(c2.summary())
    print(c3.summary())
    print(c4.summary())

    # freeze the layer for not letting retrain the model
    for layer in c1.layers:
        layer.trainable = False
    for layer in c2.layers:
        layer.trainable = False
    for layer in c3.layers:
        layer.trainable = False
    for layer in c4.layers:
        layer.trainable = False

    # get layer for the fusion
    y1 = c1.get_layer('c1_dense2').output
    z1 = c2.get_layer('c2_dense2').output
    x1 = c3.get_layer('c3_dense2').output
    m1 = c4.get_layer('c4_dense2').output

    # cnn models
    c1_ = keras.models.Model(inputs=c1.input, outputs=y1)
    c2_ = keras.models.Model(inputs=c2.input, outputs=z1)
    c3_ = keras.models.Model(inputs=c3.input, outputs=x1)
    c4_ = keras.models.Model(inputs=c4.input, outputs=m1)

    # concatenate the output
    # extract the features
    y = c1_(input_layer)
    z = c2_(input_layer)
    x = c3_(input_layer)
    m = c4_(input_layer)

    k = Concatenate()([y, z, x, m])

    y = Dropout(0.5)(k)

    x = Dense(128, activation='relu')(y)
    y = Dropout(0.5)(x)

    x = Dense(64, activation='relu')(y)
    x = Dropout(0.5)(x)

    x = Dense(3, activation='softmax')(x)

    print(x.shape)
    model = keras.models.Model(inputs=input_layer, outputs=x)
    opt = keras.optimizers.Adam(lr=0.0001)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['acc'])
    return model


def custom_layer(tensor):
    return (tensor[0] + tensor[1] + tensor[2] + tensor[3])/4
    #return max([tensor[0], tensor[1], tensor[2], tensor[3]],axis=0)


def fusion_model(set):
    # design four models
   # input_layer = Input((403, 1))

    # load the models
    c1 = load_model(set + '/c1.h5')
    c2 = load_model(set + '/c2.h5')
    c3 = load_model(set + '/c3.h5')
    c4 = load_model(set + '/c4.h5')
    # print(c1.input)

    # Load channel summary
    print(c1.summary())
    print(c2.summary())
    print(c3.summary())
    print(c4.summary())

    # freeze the layer for not letting retrain the model
    for layer in c1.layers[:-1]: #71.33 for [:-1] with average voting
        layer.trainable = True
    for layer in c2.layers[:-1]:
        layer.trainable = True
    for layer in c3.layers[:-1]:
        layer.trainable = True
    for layer in c4.layers[:-1]:
        layer.trainable = True

    print(c1.summary())
    print(c2.summary())
    print(c3.summary())
    print(c4.summary())

    decision_fusion_ = ([c1.output, c2.output, c3.output, c4.output])

    decision_fusion_ = Lambda(custom_layer, name="decision_fusion")(decision_fusion_)

    # decision_fusion_=average([decision_fusion_,decision_fusion1_])
    # decision_fusion_ = Dense(3, activation='softmax')(decision_fusion_)
    modelEns = Model(inputs=[c1.input, c2.input, c3.input, c4.input], outputs=decision_fusion_,
                     name='ensemble')
    opt = keras.optimizers.Adam(lr=0.0001)
    modelEns.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['acc'])
    modelEns.summary()
    return modelEns


def define_stacked_model(set):
    members = []

    # load the models
    c1 = load_model(set + '/c1.h5')
    c2 = load_model(set + '/c2.h5')
    c3 = load_model(set + '/c3.h5')
    c4 = load_model(set + '/c4.h5')

    members.append(c1)
    members.append(c2)
    members.append(c3)
    members.append(c4)

    for i in range(len(members)):
        model = members[i]
        for layer in model.layers:
            # make not trainable
            layer.trainable = False
            # rename to avoid 'unique layer name' issue
            layer._name = 'ensemble_' + str(i + 1) + '_' + layer.name
    # define multi-headed input
    ensemble_visible = [model.input for model in members]
    # concatenate merge output from each model
    ensemble_outputs = [model.output for model in members]
    merge = concatenate(ensemble_outputs)
    hidden = Dense(12, activation='relu')(merge)
    output = Dense(3, activation='softmax')(hidden)
    model = Model(inputs=ensemble_visible, outputs=output)
    # compile
    opt = keras.optimizers.Adam(lr=0.0001)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
    return model


N_SETS = 10

if __name__ == '__main__':

    read_main_path1 = os.path.join('data', 'fold_train_test_dataset_overall_vectors_for_300dim')
    read_main_path2 = os.path.join('data', 'fold_train_test_dataset_overall_vectors_3dim')
    read_main_path3 = os.path.join('data', 'fold_train_test_dataset_overall_vectors')
    read_main_path4 = os.path.join('data', 'processing', 'tfidf_vector')
    model_write_path = os.path.join('data', 'pre-trained-models')

    data_dict = dict()
    accuracy = []

    for n_set in range(N_SETS):
        print("---------------- Loading Set {} data sets -------------------".format(n_set + 1))
        read_joiner_path1 = os.path.join(read_main_path1, 'set_' + str(n_set + 1))
        read_joiner_path2 = os.path.join(read_main_path2, 'set_' + str(n_set + 1))
        read_joiner_path3 = os.path.join(read_main_path3, 'set_' + str(n_set + 1))
        read_joiner_path4 = os.path.join(read_main_path4, 'set_' + str(n_set + 1))
        writer_joiner_path = os.path.join(model_write_path, 'set_' + str(n_set + 1))

        mkdir(writer_joiner_path)

        train_x1 = read_pickle_data(os.path.join(read_joiner_path1, 'train_x.pkl'))
        train_y1 = read_pickle_data(os.path.join(read_joiner_path1, 'train_y.pkl'))
        test_x1 = read_pickle_data(os.path.join(read_joiner_path1, 'test_x.pkl'))
        test_y1 = read_pickle_data(os.path.join(read_joiner_path1, 'test_y.pkl'))

        train_x2 = read_pickle_data(os.path.join(read_joiner_path2, 'train_x.pkl'))
        train_y2 = read_pickle_data(os.path.join(read_joiner_path2, 'train_y.pkl'))
        test_x2 = read_pickle_data(os.path.join(read_joiner_path2, 'test_x.pkl'))
        test_y2 = read_pickle_data(os.path.join(read_joiner_path2, 'test_y.pkl'))

        train_x3 = read_pickle_data(os.path.join(read_joiner_path3, 'train_x.pkl'))
        train_y3 = read_pickle_data(os.path.join(read_joiner_path3, 'train_y.pkl'))
        test_x3 = read_pickle_data(os.path.join(read_joiner_path3, 'test_x.pkl'))
        test_y3 = read_pickle_data(os.path.join(read_joiner_path3, 'test_y.pkl'))

        train_x4 = read_pickle_data(os.path.join(read_joiner_path4, 'train_x.pkl'))
        train_y4 = read_pickle_data(os.path.join(read_joiner_path4, 'train_y.pkl'))
        test_x4 = read_pickle_data(os.path.join(read_joiner_path4, 'test_x.pkl'))
        test_y4 = read_pickle_data(os.path.join(read_joiner_path4, 'test_y.pkl'))

        # train_x=np.concatenate((train_x1,train_x2),axis=1)
        # train_y=np.concatenate((train_y1,train_y2),axis=1)
        # test_x= np.concatenate((test_x1,test_x2),axis=1)
        # test_y= np.concatenate((test_y1,test_y2),axis=1)

        # tr = np.array(train_x1)
        # tr_ = np.array(train_x4)
        #
        # print(tr.shape)
        # print(tr_.shape)

        train_x = np.concatenate((np.array(train_x1), np.array(train_x2), np.array(train_x4)), axis=1)
        train_y = np.array(train_y1)
        test_x = np.concatenate((np.array(test_x1), np.array(test_x2), np.array(test_x4)), axis=1)
        test_y = np.array(test_y1)

        print("---------------- Loading Set {} completed -------------------".format(n_set + 1))

        print(np.shape(train_x), np.shape(train_y))
        print(np.shape(test_x), np.shape(test_y))

        scale_model = StandardScaler()
        scale_model.fit(train_x)

        train_x = scale_model.transform(train_x)
        test_x = scale_model.transform(test_x)

        print(np.shape(train_x), np.shape(train_y))
        print(np.shape(test_x), np.shape(test_y))

        le = preprocessing.LabelEncoder()
        le.fit(train_y)

        train_y = le.transform(train_y)
        test_y = le.transform(test_y)

        train_y = to_categorical(train_y)
        test_y = to_categorical(test_y)

        train_x = np.array(train_x).reshape((np.shape(train_x)[0], np.shape(train_x)[1], 1))
        test_x = np.array(test_x).reshape((np.shape(test_x)[0], np.shape(test_x)[1], 1))

        print(np.shape(train_x), np.shape(train_y))
        print(np.shape(test_x), np.shape(test_y))
        # model = cnn2d_model_for_17features()
        # model = multichannel_network()
        # model = combined_model(writer_joiner_path)
        model = fusion_model(writer_joiner_path)

        #model = define_stacked_model(writer_joiner_path)
        print(model.summary())

        # simple early stopping
        es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=5)
        # mc = ModelCheckpoint(os.path.join(writer_joiner_path, str(n_set + 1) + '_mean_ensemble.h5'),
        #                      monitor='val_accuracy', mode='max', verbose=1, save_best_only=True)

        print('---------batch size : {}----------------'.format(32))
        history = model.fit([train_x, train_x, train_x, train_x], train_y,
                            # validation_split=0.3,
                            validation_data=([test_x, test_x, test_x, test_x], test_y),
                            epochs=50, batch_size=64, callbacks=[es])
        # history = model.fit(train_x, train_y, validation_data=(test_x, test_y),
        #                     epochs=50, batch_size=64, callbacks=[es])
        # history = model.fit(train_x, train_y, validation_split=0.3,
        #                                          epochs=50, batch_size=64, callbacks=[es])

        print("Evaluate on test data")
        results = model.evaluate([test_x, test_x, test_x, test_x], test_y, batch_size=64)
        # results = model.evaluate(test_x, test_y, batch_size=64)
        # model.save(os.path.join(writer_joiner_path, str(n_set + 1) + 'c1.h5'))
        # model.save(os.path.join(writer_joiner_path, 'c4.h5'))
        print("test loss, test acc:", results)
        data_dict[str(n_set + 1)] = [results[1] * 100]
        accuracy.append(results[1] * 100)
    data_dict['avg'] = np.mean(accuracy)
    print("Average values is : ", np.mean(accuracy))
    # df = pd.DataFrame(data_dict)
    # df.to_csv(os.path.join('data', 'model_save_update', 'result_17_dim.csv'))
    # predict = model.predict(test_x)
    # predict_ = np.zeros_like(predict)
    # predict_[np.arange(len(predict)), predict.argmax(1)] = 1
    #
    # print(classification_report(test_y, predict_))
    #
    #
    # from matplotlib import pyplot
    #
    # # learning curves of model accuracy
    # pyplot.plot(history.history['acc'], label='train')
    # pyplot.plot(history.history['val_acc'], label='test')
    # pyplot.legend()
    # pyplot.show()
    # exit(0)
