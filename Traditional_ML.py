import os
import pickle
import pandas as pd
import numpy as np
import tensorflow as tf
from sklearn import preprocessing
from sklearn.metrics import classification_report
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import precision_score, recall_score, f1_score, accuracy_score, classification_report,confusion_matrix
from sklearn.model_selection import GridSearchCV
from xgboost import XGBClassifier
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
import xgboost as xgb


# 1==pos
# 0=neutral
# -1==neg
#
#
# 2==pos
# 0==neg
# 1==neutral

def read_pickle_data(path=None):
    if path is None or not os.path.isfile(path):
        return None
    with open(path, 'rb') as fid:
        data = pickle.load(fid)
        fid.close()
        return data


def mkdir(path):
    if not os.path.exists(path):
        try:
            os.makedirs(path)
        except NotADirectoryError:
            pass

if __name__ == '__main__':

    N_SETS = 10
    read_main_path1 = os.path.join('data', 'fold_train_test_dataset_overall_vectors_for_300dim')
    read_main_path2 = os.path.join('data', 'fold_train_test_dataset_overall_vectors_3dim')
    read_main_path3 = os.path.join('data', 'fold_train_test_dataset_overall_vectors')
    read_main_path4 = os.path.join('data', 'processing', 'tfidf_vector')
    model_write_path = os.path.join('data', 'pre-trained-models')

    data_dict = dict()
    accuracy = []
    precision = []
    recall = []
    fscore = []

    for n_set in range(N_SETS):
        print("---------------- Loading Set {} data sets -------------------".format(n_set + 1))
        read_joiner_path1 = os.path.join(read_main_path1, 'set_' + str(n_set + 1))
        read_joiner_path2 = os.path.join(read_main_path2, 'set_' + str(n_set + 1))
        read_joiner_path3 = os.path.join(read_main_path3, 'set_' + str(n_set + 1))
        read_joiner_path4 = os.path.join(read_main_path4, 'set_' + str(n_set + 1))
        writer_joiner_path = os.path.join(model_write_path, 'set_' + str(n_set + 1))

        mkdir(writer_joiner_path)

        train_x1 = read_pickle_data(os.path.join(read_joiner_path1, 'train_x.pkl'))
        train_y1 = read_pickle_data(os.path.join(read_joiner_path1, 'train_y.pkl'))
        test_x1 = read_pickle_data(os.path.join(read_joiner_path1, 'test_x.pkl'))
        test_y1 = read_pickle_data(os.path.join(read_joiner_path1, 'test_y.pkl'))

        train_x2 = read_pickle_data(os.path.join(read_joiner_path2, 'train_x.pkl'))
        train_y2 = read_pickle_data(os.path.join(read_joiner_path2, 'train_y.pkl'))
        test_x2 = read_pickle_data(os.path.join(read_joiner_path2, 'test_x.pkl'))
        test_y2 = read_pickle_data(os.path.join(read_joiner_path2, 'test_y.pkl'))

        train_x3 = read_pickle_data(os.path.join(read_joiner_path3, 'train_x.pkl'))
        train_y3 = read_pickle_data(os.path.join(read_joiner_path3, 'train_y.pkl'))
        test_x3 = read_pickle_data(os.path.join(read_joiner_path3, 'test_x.pkl'))
        test_y3 = read_pickle_data(os.path.join(read_joiner_path3, 'test_y.pkl'))

        train_x4 = read_pickle_data(os.path.join(read_joiner_path4, 'train_x.pkl'))
        train_y4 = read_pickle_data(os.path.join(read_joiner_path4, 'train_y.pkl'))
        test_x4 = read_pickle_data(os.path.join(read_joiner_path4, 'test_x.pkl'))
        test_y4 = read_pickle_data(os.path.join(read_joiner_path4, 'test_y.pkl'))

        train_x = np.concatenate((np.array(train_x1), np.array(train_x2), np.array(train_x4)), axis=1)
        train_y = np.array(train_y1)
        test_x = np.concatenate((np.array(test_x1), np.array(test_x2), np.array(test_x4)), axis=1)
        test_y = np.array(test_y1)

        print("---------------- Loading Set {} completed -------------------".format(n_set + 1))

        print(np.shape(train_x), np.shape(train_y))
        print(np.shape(test_x), np.shape(test_y))

        scale_model = StandardScaler()
        scale_model.fit(train_x)

        train_x = scale_model.transform(train_x)
        test_x = scale_model.transform(test_x)

        print(np.shape(train_x), np.shape(train_y))
        print(np.shape(test_x), np.shape(test_y))
        print(test_y)
        # te=pd.DataFrame(test_y)
        # te.to_csv('test_y.csv')

        le = preprocessing.LabelEncoder()
        le.fit(train_y)

        train_y = le.transform(train_y)
        test_y = le.transform(test_y)
        # print(test_y)
        # te=pd.DataFrame(test_y)
        # te.to_csv('test_y1.csv')

        # train_y = to_categorical(train_y)
        # test_y = to_categorical(test_y)
        print(test_y)

        # train_x = np.array(train_x).reshape((np.shape(train_x)[0], np.shape(train_x)[1], 1))
        # test_x = np.array(test_x).reshape((np.shape(test_x)[0], np.shape(test_x)[1], 1))

        print(np.shape(train_x), np.shape(train_y))
        print(np.shape(test_x), np.shape(test_y))

        # NB
        # gnb = GaussianNB(priors=None, var_smoothing=1)
        # gnb.fit(train_x, train_y)
        # predict = gnb.predict(test_x)

        # # ANN
        # clf = MLPClassifier(hidden_layer_sizes=(60,), learning_rate_init=0.01, max_iter=1000, random_state=1)
        # clf.fit(train_x, train_y)
        # predict = clf.predict(test_x)
        #
        # # KNN
        # neigh = KNeighborsClassifier(leaf_size=35, n_neighbors=120, p=1)
        # neigh.fit(train_x, train_y)
        # predict = neigh.predict(test_x)
        #
        # # LINEARSVM
        # svc = SVC(C=10, kernel='linear', gamma=0.1, random_state=1, verbose=2)
        # svc.fit(train_x, train_y)
        # print("Evaluating Results..")
        # predict = svc.predict(test_x)
        #
        # # LOGISTIC REGRESSION
        # clf = LogisticRegression(random_state=1, C=1000, solver="lbfgs", max_iter=500)
        # clf.fit(train_x, train_y)
        # predict = clf.predict(test_x)
        #
        # # RANDOM FOREST
        # rf = RandomForestClassifier(random_state=1, min_samples_leaf=3, min_samples_split=6,
        #                             n_estimators=200)
        # rf.fit(train_x, train_y)
        # predict = rf.predict(test_x)
        #
        # # RBF
        # svc = SVC(kernel='rbf', class_weight='balanced', C=100, gamma=1)
        # svc.fit(train_x, train_y)
        # predict = svc.predict(test_x)
        #
        # # XGB
        xgb_model = xgb.XGBClassifier(learning_rate=0.1, max_depth=7, n_estimators=150)
        xgb_model.fit(train_x, train_y)
        predict = xgb_model.predict(test_x)

        # model = load_model(writer_joiner_path + '/' + 'c1.h5')
        #
        # # model = define_stacked_model(writer_joiner_path)
        # print(model.summary())
        # print("Evaluate on test data")
        # # results = model.evaluate([test_x], test_y, batch_size=64)
        # # results = model.evaluate(test_x, test_y, batch_size=64)
        # # model.save(os.path.join(writer_joiner_path, str(n_set + 1) + 'c1.h5'))
        # # model.save(os.path.join(writer_joiner_path, 'c4.h5'))
        # # print("test loss, test acc:", results)
        #
        # # predict probabilities for test set
        # # yhat_probs = model.predict(test_x, verbose=0)
        # # # predict crisp classes for test set
        # # # yhat_classes = model.predict_classes(test_x, verbose=0)
        # # #yhat_classes = np.argmax(yhat_probs, axis=1)
        # # print(test_y)
        # # yhat_classes=test_y
        #
        # predict = model.predict(test_x)
        # # predict = np.zeros_like(predict)
        # # predict[np.arange(len(predict)), predict.argmax(1)] = 1
        # predict = np.argmax(predict, axis=1)
        #
        # # predict=predict.round()
        # # predict=np.argmax(predict,axis=1)
        # print(predict)
        # print(test_y)

        # reduce to 1d array
        # yhat_probs = yhat_probs[:, 0]
        # yhat_classes = yhat_classes[:, 0]

        # accuracy: (tp + tn) / (p + n)
        acc = accuracy_score(test_y, predict)
        print('Accuracy: %f' % acc)
        # precision tp / (tp + fp)
        prec = precision_score(test_y, predict, average='macro')
        print('Precision: %f' % prec)
        # recall: tp / (tp + fn)
        rec = recall_score(test_y, predict, average='macro')
        print('Recall: %f' % rec)
        # f1: 2 tp / (2 tp + fp + fn)
        f1 = f1_score(test_y, predict, average='macro')
        print('F1 score: %f' % f1)

        # data_dict[str(n_set+1)]=
        accuracy.append(acc)
        precision.append(prec)
        recall.append(rec)
        fscore.append(f1)
        print(classification_report(test_y, predict,target_names=['Negative','Neutral','Positive']))
        print(confusion_matrix(test_y,predict))

    # data_dict['avg'] = np.mean(accuracy)
    print("Avg. Accuracy is : ", np.mean(accuracy))
    print("Avg. Precision is :", np.mean(precision))
    print('Avg. Recall is :', np.mean(recall))
    print('Avg. F1-score is :', np.mean(fscore))
