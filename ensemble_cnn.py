
import os

import tensorflow as tf

from sklearn import preprocessing
from sklearn.metrics import f1_score, precision_score, recall_score, accuracy_score
from tensorflow import keras
from tensorflow.python.keras._impl.keras.models import load_model
from tensorflow.python.keras._impl.keras.utils import to_categorical
from tensorflow.python.keras.layers import Lambda, LSTM, Conv1D, Activation, Dropout, MaxPooling1D, Flatten, Dense, \
    average, \
    maximum, multiply, TimeDistributed, Reshape
from tensorflow.python.keras.callbacks import EarlyStopping, LearningRateScheduler, ModelCheckpoint
import h5py


def mkdir(path):
    if not os.path.exists(path):
        try:
            os.makedirs(path)
        except NotADirectoryError:
            pass


def get_data_split(data):
    x = []
    y = []
    for key, value in data.items():
        target = list(value.keys())[0]
        x.append(value[target])
        y.append(target)
    return x, y


def read_pickle_data(path=None):
    import pickle
    with open(path, 'rb') as fid:
        data = pickle.load(fid)
        fid.close()
        return data


from tensorflow.python.keras.layers import concatenate, add
from tensorflow.python.keras import Model, Sequential

import pandas as pd
from sklearn.preprocessing import LabelEncoder, StandardScaler
import numpy as np

N_SETS = 10

if __name__ == '__main__':
    model_path = os.path.join('data', 'model_save_update')

    read_main_path_3D = os.path.join('data', 'fold_train_test_dataset_overall_vectors_3dim')
    read_main_path_17D = os.path.join('data', 'fold_train_test_dataset_overall_vectors')
    # read_main_path_300D = os.path.join('data', 'fold_train_test_dataset_overall_vectors_3dim')
    read_main_path_300D = os.path.join('data', 'fold_train_test_dataset_overall_vectors_for_300dim')

    model_write_path = os.path.join('data', 'model_save_ensemble_update')

    data_dict = dict()
    accuracy = []

    f_scores = []
    p_scores = []
    a_scores = []
    r_scores = []

    data_structure = {
        'sets': [],
        'f1_score': [],
        'precision_score': [],
        'recall_score': [],
        'accuracy_score': [],
    }
    for n_set in range(N_SETS):
        print("---------------- Loading Set {} data sets -------------------".format(n_set + 1))
        model_path_joiner = os.path.join(model_path, 'set_' + str(n_set + 1))

        read_joiner_path_3D = os.path.join(read_main_path_3D, 'set_' + str(n_set + 1))
        read_joiner_path_17D = os.path.join(read_main_path_17D, 'set_' + str(n_set + 1))
        read_joiner_path_300D = os.path.join(read_main_path_300D, 'set_' + str(n_set + 1))

        writer_joiner_path = os.path.join(model_write_path, 'set_' + str(n_set + 1))

        mkdir(writer_joiner_path)

        train_x_3D = read_pickle_data(os.path.join(read_joiner_path_3D, 'train_x.pkl'))
        train_y_3D = read_pickle_data(os.path.join(read_joiner_path_3D, 'train_y.pkl'))
        test_x_3D = read_pickle_data(os.path.join(read_joiner_path_3D, 'test_x.pkl'))
        test_y_3D = read_pickle_data(os.path.join(read_joiner_path_3D, 'test_y.pkl'))

        train_x_17D = read_pickle_data(os.path.join(read_joiner_path_17D, 'train_x.pkl'))
        train_y_17D = read_pickle_data(os.path.join(read_joiner_path_17D, 'train_y.pkl'))
        test_x_17D = read_pickle_data(os.path.join(read_joiner_path_17D, 'test_x.pkl'))
        test_y_17D = read_pickle_data(os.path.join(read_joiner_path_17D, 'test_y.pkl'))

        train_x_300D = read_pickle_data(os.path.join(read_joiner_path_300D, 'train_x.pkl'))
        train_y_300D = read_pickle_data(os.path.join(read_joiner_path_300D, 'train_y.pkl'))
        test_x_300D = read_pickle_data(os.path.join(read_joiner_path_300D, 'test_x.pkl'))
        test_y_300D = read_pickle_data(os.path.join(read_joiner_path_300D, 'test_y.pkl'))

        print("---------------- Loading Set {} completed -------------------".format(n_set + 1))

        print(np.shape(train_x_3D), np.shape(train_y_3D))
        print(np.shape(test_x_3D), np.shape(test_y_3D))
        print(np.shape(train_x_17D), np.shape(train_y_17D))
        print(np.shape(test_x_17D), np.shape(test_y_17D))
        print(np.shape(train_x_300D), np.shape(train_y_300D))
        print(np.shape(test_x_300D), np.shape(test_y_300D))

        scale_model = StandardScaler()
        scale_model.fit(train_x_3D)

        train_x_3D = scale_model.transform(train_x_3D)
        test_x_3D = scale_model.transform(test_x_3D)

        print(np.shape(train_x_3D), np.shape(train_y_3D))
        print(np.shape(test_x_3D), np.shape(test_y_3D))

        scale_model_17D = StandardScaler()
        scale_model_17D.fit(train_x_17D)

        train_x_17D = scale_model_17D.transform(train_x_17D)
        test_x_17D = scale_model_17D.transform(test_x_17D)

        print(np.shape(train_x_17D), np.shape(train_y_17D))
        print(np.shape(test_x_17D), np.shape(test_y_17D))

        scale_model_300D = StandardScaler()
        scale_model_300D.fit(train_x_300D)

        train_x_300D = scale_model_300D.transform(train_x_300D)
        test_x_300D = scale_model_300D.transform(test_x_300D)

        print(np.shape(train_x_300D), np.shape(train_y_300D))
        print(np.shape(test_x_300D), np.shape(test_y_300D))

        le = preprocessing.LabelEncoder()
        le.fit(train_y_3D)

        train_y_3D = le.transform(train_y_3D)
        test_y_3D = le.transform(test_y_3D)

        train_y_3D = to_categorical(train_y_3D)
        test_y_3D = to_categorical(test_y_3D)

        train_x_3D = np.array(train_x_3D).reshape((np.shape(train_x_3D)[0], np.shape(train_x_3D)[1], 1))
        test_x_3D = np.array(test_x_3D).reshape((np.shape(test_x_3D)[0], np.shape(test_x_3D)[1], 1))

        print(np.shape(train_x_3D), np.shape(train_y_3D))
        print(np.shape(test_x_3D), np.shape(test_y_3D))

        le_17 = preprocessing.LabelEncoder()
        le_17.fit(train_y_17D)

        train_y_17D = le_17.transform(train_y_17D)
        test_y_17D = le_17.transform(test_y_17D)

        train_y_17D = to_categorical(train_y_17D)
        test_y_17D = to_categorical(test_y_17D)

        train_x_17D = np.array(train_x_17D).reshape((np.shape(train_x_17D)[0], np.shape(train_x_17D)[1], 1))
        test_x_17D = np.array(test_x_17D).reshape((np.shape(test_x_17D)[0], np.shape(test_x_17D)[1], 1))

        print(np.shape(train_x_17D), np.shape(train_y_17D))
        print(np.shape(test_x_17D), np.shape(test_y_17D))

        le_300D = preprocessing.LabelEncoder()
        le_300D.fit(train_y_300D)

        train_y_300D = le_300D.transform(train_y_300D)
        test_y_300D = le_300D.transform(test_y_300D)

        train_y_300D = to_categorical(train_y_300D)
        test_y_300D = to_categorical(test_y_300D)

        train_x_300D = np.array(train_x_300D).reshape((np.shape(train_x_300D)[0], np.shape(train_x_300D)[1], 1))
        test_x_300D = np.array(test_x_300D).reshape((np.shape(test_x_300D)[0], np.shape(test_x_300D)[1], 1))

        print(np.shape(train_x_300D), np.shape(train_y_300D))
        print(np.shape(test_x_300D), np.shape(test_y_300D))

 

        model_300D = load_model(
            os.path.join('data', 'model_save_update', 'set_' + str(n_set + 1), str(n_set + 1) + '_300dim_rmsprop.h5'))
        model_17D = load_model(
            os.path.join('data', 'model_save_update', 'set_' + str(n_set + 1), str(n_set + 1) + '_17dim_rmsprop.h5'))
        model_3D = load_model(
            os.path.join('data', 'model_save_update', 'set_' + str(n_set + 1), str(n_set + 1) + '_3dim_rmsprop.h5'))

        for layer in model_300D.layers[0:-3]:
            layer.trainable = False

        for layer in model_3D.layers[0:-3]:
            layer.trainable = False

        for layer in model_17D.layers[0:-3]:
            layer.trainable = False

        for layer in model_300D.layers:
            print(layer.trainable)

        print(model_300D.summary())
        print(model_3D.summary())
        print(model_17D.summary())

        print(model_300D.output)
        print(model_3D.output)
        print(model_17D.output)


        def custom_layer(tensor):
            return tensor[0] * 0.70 + tensor[1] * 0.10 + tensor[2] * 0.20


        decision_fusion_ = ([model_300D.output, model_17D.output, model_3D.output])

        # decision_fusion1_ = maximum([model_300D.output, model_17D.output, model_3D.output])
        # decision_fusion_=Reshape(target_shape=(9,1))(decision_fusion_)
        # decision_fusion_=LSTM(9,return_state=False)(decision_fusion_)
        # decision_fusion_=Dropout(0.5)(decision_fusion_)
        # decision_fusion_=Dense(64,activation='relu')(decision_fusion_)
        # decision_fusion_ = ([three_hundred_layer, seventeen_layer, three_layer])
        decision_fusion_ = Lambda(custom_layer, name="decision_fusion")(decision_fusion_)

        # decision_fusion_=average([decision_fusion_,decision_fusion1_])
        # decision_fusion_ = Dense(3, activation='softmax')(decision_fusion_)
        modelEns = Model(inputs=[model_300D.input, model_17D.input, model_3D.input], outputs=decision_fusion_,
                         name='ensemble')

        modelEns.summary()

        # simple early stopping
        es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=5)
        mc = ModelCheckpoint(os.path.join(writer_joiner_path, str(n_set + 1) + '_mean_ensemble.h5'),
                             monitor='val_accuracy', mode='max', verbose=1, save_best_only=True)
        modelEns.compile(loss='categorical_crossentropy',
                         optimizer=tf.keras.optimizers.RMSprop(lr=0.00001),
                         metrics=['acc'])

        # train the model
        print("[INFO] training model...")
        modelEns.fit(
            x=[train_x_300D, train_x_17D, train_x_3D], y=train_y_3D,
            validation_split=0.3,
            # validation_data=([test_x_300D, test_x_17D,test_x_3D], test_y_3D),
            epochs=50, batch_size=32, callbacks=[es, mc])

        print("Evaluate on test data")
        results = modelEns.evaluate([test_x_300D, test_x_17D, test_x_3D], test_y_3D, batch_size=32)
        modelEns.save(os.path.join(writer_joiner_path, str(n_set + 1) + '_mean_ensemble.h5'))

        # model = Sequential()
        # model.add(Conv1D(filters=128, kernel_size=3, input_shape=(320, 1), activation='relu'))
        # model.add(Conv1D(filters=128, kernel_size=3, activation='relu'))
        # model.add(MaxPooling1D(pool_size=2))
        # model.add(Flatten())
        # model.add(Dropout(0.2))
        # model.add(Dense(32,activation='relu'))
        # model.add(Dropout(0.2))
        # model.add(Dense(16, activation='relu'))
        # model.add(Dense(3, activation='softmax'))
        #
        # # feature fusion
        # train_x = np.concatenate((train_x_300D, train_x_17D, train_x_3D),axis=1)
        # train_y = train_y_3D
        # test_x = np.concatenate((test_x_300D, test_x_17D, test_x_3D),axis=1)
        # test_y = test_y_3D
        #
        # print(train_x.shape)
        # print(train_y.shape)
        # print(test_x.shape)
        # print(test_y.shape)
        #
        # model.compile(loss='categorical_crossentropy',
        #                  optimizer=tf.keras.optimizers.RMSprop(lr=0.001),
        #                  metrics=['acc'])
        # model.fit(
        #     train_x, train_y,
        #     validation_data=(test_x, test_y),
        #     epochs=50, batch_size=32)
        #
        # print("Evaluate on test data")
        # results = model.evaluate(test_x, test_y, batch_size=32)
        # results = model.evaluate([test_x_300D, test_x_17D, test_x_3D], test_y_3D, batch_size=32)
        # model.save(os.path.join(writer_joiner_path, str(n_set + 1) + '_mean_ensemble.h5'))
        #
        print("test loss, test acc:", results)
        data_dict[str(n_set + 1)] = [results[1] * 100]
        accuracy.append(results[1] * 100)
        predict = modelEns.predict([test_x_300D, test_x_17D, test_x_3D])
        predict_ = np.zeros_like(predict)
        predict_[np.arange(len(predict)), predict.argmax(1)] = 1

        f_scores.append(f1_score(test_y_3D, predict_, average=None))
        p_scores.append(precision_score(test_y_3D, predict_, average=None))
        r_scores.append(recall_score(test_y_3D, predict_, average=None))
        a_scores.append(accuracy_score(test_y_3D, predict_))

        data_structure['sets'].append('set_' + str(n_set + 1))
        data_structure['f1_score'].append(f1_score(test_y_3D, predict_, average='weighted'))
        data_structure['precision_score'].append(precision_score(test_y_3D, predict_, average='weighted'))
        data_structure['recall_score'].append(recall_score(test_y_3D, predict_, average='weighted'))
        data_structure['accuracy_score'].append(accuracy_score(test_y_3D, predict_))
    print('P-Score : ', np.mean(p_scores, axis=0))
    print('R-Score : ', np.mean(r_scores, axis=0))
    print('F-score : ', np.mean(f_scores, axis=0))

    print('Accuracy : ', np.mean(a_scores))

    data_structure['sets'].append('average')
    data_structure['f1_score'].append(np.mean(data_structure['f1_score']))
    data_structure['precision_score'].append(np.mean(data_structure['precision_score']))
    data_structure['recall_score'].append(np.mean(data_structure['recall_score']))
    data_structure['accuracy_score'].append(np.mean(data_structure['accuracy_score']))
    import pandas as pd

    df = pd.DataFrame(data_structure)
    df.to_csv('data//result_ensemble.csv')

    data_dict['avg'] = np.mean(accuracy)
    print("Average values is : ", np.mean(accuracy))
    df = pd.DataFrame(data_dict)
    df.to_csv(os.path.join('data', 'model_save_ensemble', 'result_dim.csv'))
