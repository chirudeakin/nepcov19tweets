import os
import pickle

from tensorflow import keras
import pandas as pd
import numpy as np
import tensorflow as tf
from keras.utils import to_categorical
from sklearn import preprocessing
from sklearn.metrics import classification_report
from sklearn.preprocessing import StandardScaler
from tensorflow.python.keras import Sequential, Input
from tensorflow.python.keras.layers import Flatten, Dense, Dropout, Conv1D, LSTM, Bidirectional, Concatenate, \
    MaxPooling1D, AveragePooling1D, TimeDistributed, GlobalAvgPool1D, GlobalMaxPool1D, multiply, average, AvgPool1D, \
    concatenate
from tensorflow.python.keras.callbacks import EarlyStopping, LearningRateScheduler, ModelCheckpoint
from tensorflow.python.keras.models import load_model, Model
from tensorflow.python.keras.layers import Lambda
from tensorflow.python.keras.backend import max
from sklearn.metrics import precision_score, recall_score, f1_score, accuracy_score, classification_report


def read_pickle_data(path=None):
    if path is None or not os.path.isfile(path):
        return None
    with open(path, 'rb') as fid:
        data = pickle.load(fid)
        fid.close()
        return data


def mkdir(path):
    if not os.path.exists(path):
        try:
            os.makedirs(path)
        except NotADirectoryError:
            pass


def multichannel_network():
    input_layer = Input((403, 1), name="Input3d_Conv")

    # block 1
    y = Conv1D(filters=32, kernel_size=1, activation='relu')(input_layer)
    y = Conv1D(filters=16, kernel_size=1, activation='relu', padding='same')(y)
    y_ = Dropout(0.4)(y)
    y = MaxPooling1D(pool_size=1)(y_)
    # y_ = AveragePooling1D(pool_size=1)(y_)
    # y = Concatenate()([y, y_])
    y = Flatten()(y)

    # block 2
    z = Conv1D(filters=32, kernel_size=2, activation='relu')(input_layer)
    z = Conv1D(filters=16, kernel_size=2, activation='relu', padding='same')(z)
    z_ = Dropout(0.4)(z)
    z = MaxPooling1D(pool_size=1)(z_)
    # z_ = AveragePooling1D(pool_size=1)(z_)
    # z = Concatenate()([z, z_])
    z = Flatten()(z)

    # block 3
    # # y=Dropout(0.1)(y)
    x = Conv1D(filters=32, kernel_size=3, activation='relu')(input_layer)
    x = Conv1D(filters=16, kernel_size=3, activation='relu', padding='same')(x)
    x_ = Dropout(0.4)(x)
    x = MaxPooling1D(pool_size=1)(x_)
    # x_ = AveragePooling1D(pool_size=1)(x_)
    # x = Concatenate()([x, x_])
    x = Flatten()(x)

    # block 4
    m = Conv1D(filters=32, kernel_size=4, activation='relu')(input_layer)
    m = Conv1D(filters=16, kernel_size=4, activation='relu', padding='same')(m)
    m_ = Dropout(0.4)(m)
    m = MaxPooling1D(pool_size=1)(m_)
    # m_ = AveragePooling1D(pool_size=1)(m_)
    # m = Concatenate()([m, m_])
    m = Flatten()(m)

    k = Concatenate()([y, z, x, m])

    y = Dropout(0.5)(k)

    x = Dense(128, activation='relu')(y)
    y = Dropout(0.5)(x)

    x = Dense(64, activation='relu')(y)
    x = Dropout(0.5)(x)

    x = Dense(3, activation='softmax')(x)
    model = keras.models.Model(inputs=input_layer, outputs=x)
    opt = keras.optimizers.Adam(lr=0.0001)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['acc'])
    return model


if __name__ == '__main__':

    N_SETS = 10
    read_main_path1 = os.path.join('data', 'fold_train_test_dataset_overall_vectors_for_300dim')
    read_main_path2 = os.path.join('data', 'fold_train_test_dataset_overall_vectors_3dim')
    read_main_path3 = os.path.join('data', 'fold_train_test_dataset_overall_vectors')
    read_main_path4 = os.path.join('data', 'processing', 'tfidf_vector')
    model_write_path = os.path.join('data', 'pre-trained-models')

    data_dict = dict()
    accuracy = []
    precision = []
    recall = []
    fscore = []

    for n_set in range(N_SETS):
        print("---------------- Loading Set {} data sets -------------------".format(n_set + 1))
        read_joiner_path1 = os.path.join(read_main_path1, 'set_' + str(n_set + 1))
        read_joiner_path2 = os.path.join(read_main_path2, 'set_' + str(n_set + 1))
        read_joiner_path3 = os.path.join(read_main_path3, 'set_' + str(n_set + 1))
        read_joiner_path4 = os.path.join(read_main_path4, 'set_' + str(n_set + 1))
        writer_joiner_path = os.path.join(model_write_path, 'set_' + str(n_set + 1))

        mkdir(writer_joiner_path)

        train_x1 = read_pickle_data(os.path.join(read_joiner_path1, 'train_x.pkl'))
        train_y1 = read_pickle_data(os.path.join(read_joiner_path1, 'train_y.pkl'))
        test_x1 = read_pickle_data(os.path.join(read_joiner_path1, 'test_x.pkl'))
        test_y1 = read_pickle_data(os.path.join(read_joiner_path1, 'test_y.pkl'))

        train_x2 = read_pickle_data(os.path.join(read_joiner_path2, 'train_x.pkl'))
        train_y2 = read_pickle_data(os.path.join(read_joiner_path2, 'train_y.pkl'))
        test_x2 = read_pickle_data(os.path.join(read_joiner_path2, 'test_x.pkl'))
        test_y2 = read_pickle_data(os.path.join(read_joiner_path2, 'test_y.pkl'))

        train_x3 = read_pickle_data(os.path.join(read_joiner_path3, 'train_x.pkl'))
        train_y3 = read_pickle_data(os.path.join(read_joiner_path3, 'train_y.pkl'))
        test_x3 = read_pickle_data(os.path.join(read_joiner_path3, 'test_x.pkl'))
        test_y3 = read_pickle_data(os.path.join(read_joiner_path3, 'test_y.pkl'))

        train_x4 = read_pickle_data(os.path.join(read_joiner_path4, 'train_x.pkl'))
        train_y4 = read_pickle_data(os.path.join(read_joiner_path4, 'train_y.pkl'))
        test_x4 = read_pickle_data(os.path.join(read_joiner_path4, 'test_x.pkl'))
        test_y4 = read_pickle_data(os.path.join(read_joiner_path4, 'test_y.pkl'))

        train_x = np.concatenate((np.array(train_x1), np.array(train_x2), np.array(train_x4)), axis=1)
        train_y = np.array(train_y1)
        test_x = np.concatenate((np.array(test_x1), np.array(test_x2), np.array(test_x4)), axis=1)
        test_y = np.array(test_y1)

        print("---------------- Loading Set {} completed -------------------".format(n_set + 1))

        print(np.shape(train_x), np.shape(train_y))
        print(np.shape(test_x), np.shape(test_y))

        scale_model = StandardScaler()
        scale_model.fit(train_x)

        train_x = scale_model.transform(train_x)
        test_x = scale_model.transform(test_x)

        print(np.shape(train_x), np.shape(train_y))
        print(np.shape(test_x), np.shape(test_y))

        le = preprocessing.LabelEncoder()
        le.fit(train_y)

        train_y = le.transform(train_y)
        test_y = le.transform(test_y)

        #train_y = to_categorical(train_y)
        #test_y = to_categorical(test_y)

        train_x = np.array(train_x).reshape((np.shape(train_x)[0], np.shape(train_x)[1], 1))
        test_x = np.array(test_x).reshape((np.shape(test_x)[0], np.shape(test_x)[1], 1))

        print(np.shape(train_x), np.shape(train_y))
        print(np.shape(test_x), np.shape(test_y))
        # model = cnn2d_model_for_17features()
        # model = multichannel_network()
        # model = combined_model(writer_joiner_path)
        # model = fusion_model(writer_joiner_path)
        model = load_model(writer_joiner_path + '/' + 'c4.h5')

        # model = define_stacked_model(writer_joiner_path)
        print(model.summary())
        print("Evaluate on test data")
        # results = model.evaluate([test_x], test_y, batch_size=64)
        # results = model.evaluate(test_x, test_y, batch_size=64)
        # model.save(os.path.join(writer_joiner_path, str(n_set + 1) + 'c1.h5'))
        # model.save(os.path.join(writer_joiner_path, 'c4.h5'))
        # print("test loss, test acc:", results)

        # predict probabilities for test set
        # yhat_probs = model.predict(test_x, verbose=0)
        # # predict crisp classes for test set
        # # yhat_classes = model.predict_classes(test_x, verbose=0)
        # #yhat_classes = np.argmax(yhat_probs, axis=1)
        # print(test_y)
        # yhat_classes=test_y

        predict = model.predict(test_x)
        # predict = np.zeros_like(predict)
        # predict[np.arange(len(predict)), predict.argmax(1)] = 1
        predict=np.argmax(predict,axis=1)


        #predict=predict.round()
       # predict=np.argmax(predict,axis=1)
        print(predict)
        print(test_y)

        # reduce to 1d array
        # yhat_probs = yhat_probs[:, 0]
        # yhat_classes = yhat_classes[:, 0]

        # accuracy: (tp + tn) / (p + n)
        acc = accuracy_score(test_y, predict)
        print('Accuracy: %f' % acc)
        # precision tp / (tp + fp)
        prec = precision_score(test_y, predict,average='macro')
        print('Precision: %f' % prec)
        # recall: tp / (tp + fn)
        rec = recall_score(test_y, predict,average='macro')
        print('Recall: %f' % rec)
        # f1: 2 tp / (2 tp + fp + fn)
        f1 = f1_score(test_y, predict,average='macro')
        print('F1 score: %f' % f1)

        # data_dict[str(n_set+1)]=
        accuracy.append(acc)
        precision.append(prec)
        recall.append(rec)
        fscore.append(f1)
        print(classification_report(test_y,predict))

    # data_dict['avg'] = np.mean(accuracy)
    print("Average values is : ", np.mean(accuracy))
    print("Precision values is :", np.mean(precision))
    print('Recall values is :', np.mean(recall))
    print('F1-score values is :', np.mean(fscore))
