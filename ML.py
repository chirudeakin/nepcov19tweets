   #IMPORT PACKAGES
import pandas as pd
import os
from sklearn.metrics import precision_score, recall_score, f1_score, accuracy_score
import numpy as np
from sklearn.model_selection import GridSearchCV
from xgboost import XGBClassifier
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
import xgboost as xgb
    
    
    
    
#NB
gnb = GaussianNB(priors=None, var_smoothing=1)
gnb.fit(X_train, y_train)
y_pred = gnb.predict(X_test)

#ANN
clf = MLPClassifier(hidden_layer_sizes=(60,), learning_rate_init=0.01, max_iter=1000, random_state=1)
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)

#KNN
neigh = KNeighborsClassifier(leaf_size=35, n_neighbors=120, p=1)
neigh.fit(X_train, y_train)
y_pred = neigh.predict(X_test)


#LINEARSVM
svc = svm.SVC(C=10, kernel='linear', gamma=0.1, random_state=1, verbose=2)
svc.fit(X_train, y_train)
print("Evaluating Results..")
y_pred = svc.predict(X_test)

#LOGISTIC REGRESSION
clf = LogisticRegression(random_state=1, C=1000, solver="lbfgs", max_iter=500)
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)

#RANDOM FOREST
rf = RandomForestClassifier(random_state=1, min_samples_leaf=3, min_samples_split=6,
                                n_estimators=200)
rf.fit(X_train, y_train)
y_pred = rf.predict(X_test)


#RBF
svc = svm.SVC(kernel='rbf', class_weight='balanced', C=100, gamma=1)
svc.fit(X_train, y_train)
y_pred = svc.predict(X_test)


#XGB
xgb_model = xgb.XGBClassifier(learning_rate=0.1, max_depth=7, n_estimators=150)
xgb_model.fit(np.array(X_train), np.array(y_train))
y_pred = xgb_model.predict(np.array(X_test))


print('Precision: %f' % precision_score(y_test, y_pred, zero_division=0, average='weighted'))
print('Accuracy: %f' % accuracy_score(y_test, y_pred))
print('Recall: %f' % recall_score(y_test, y_pred, average='weighted'))
print('F1 Score: %f' % f1_score(y_test, y_pred, average='weighted'))
print(classification_report(y_test, y_pred))